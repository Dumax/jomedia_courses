<?php
/**
 * @author Ivan Dmytrenko <ivan.dmytrenko.ua@gmail.com>
 */

namespace JoMedia\File;

interface FileInterface
{
    /**
     * Gets file size.
     *
     * @return int Returns the size of the file in bytes.
     */
    public function getSize();

    /**
     * Gets file permissions.
     *
     * @return string Returns a file permissions as letters (e.g. -rwxrw-r--).
     */
    public function getPerms();

    /**
     * Tells is a file available for reading.
     *
     * @return bool Returns TRUE if a file is readable, FALSE otherwise.
     */
    public function isReadable();

    /**
     * Tells is a file available for writing.
     *
     * @return bool Returns TRUE if a file is writable, FALSE otherwise.
     */
    public function isWritAble();

    /**
     * Gets last access time of file
     *
     * @return string Returns last access time of file in (30.12.2015 10:15:59) format
     */
    public function getLastAccessTime();

    /**
     * Locks or unlocks the file.
     *
     * @param $operation
     * LOCK_SH to acquire a shared lock
     * LOCK_EX to acquire an exclusive lock
     * LOCK_UN to release a lock
     *
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function lock($operation);

}