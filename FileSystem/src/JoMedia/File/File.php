<?php 

namespace JoMedia\File;

class File implements FileInterface{

    public $filename;
    public $fopen;

    public function __construct($filename, $mode = "r+")
    {
        if (file_exists($filename)) {
            $this->filename = $filename;
            $this->fopen = fopen($filename, $mode);
        } else {
           die('File doesn`t exist');
        }
    }

    public function getSize()
    {
        return filesize($this->filename);
    }

    public function getPerms()
    {
        $perms = fileperms($this->filename);

        if (($perms & 0xC000) == 0xC000) {
            // Socket
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Symbolic Link
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Regular
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Block special
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Directory
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Character special
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // FIFO pipe
            $info = 'p';
        } else {
            // Unknown
            $info = 'u';
        }

        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
                    (($perms & 0x0800) ? 's' : 'x' ) :
                    (($perms & 0x0800) ? 'S' : '-'));

        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
                    (($perms & 0x0400) ? 's' : 'x' ) :
                    (($perms & 0x0400) ? 'S' : '-'));

        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
                    (($perms & 0x0200) ? 't' : 'x' ) :
                    (($perms & 0x0200) ? 'T' : '-'));

        return $info;
    }

    public function isReadable()
    {
        return (is_readable($this->filename)) ? "TRUE" : "FALSE";
    }

    public function isWritAble()
    {
        return (is_writable($this->filename)) ? "TRUE" : "FALSE";
    }

    public function getLastAccessTime()
    {
        return date('d.m.Y H:i:s', fileatime($this->filename));
    }

    public function lock($operation)
    {
        return (flock($this->fopen, $operation)) ? "TRUE" : "FALSE";
    }

}