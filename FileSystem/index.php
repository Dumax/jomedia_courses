<?php 

require_once 'vendor/autoload.php';

use JoMedia\File\File;


$file = new File("test.txt","r+");

echo "File size: ",$file->getSize()," byte.<br>";
echo "File permisions: ",$file->getPerms(),".<br>";
echo "File is readable: ",$file->isReadable(),".<br>";
echo "File is writable: ",$file->isWritAble(),".<br>";
echo "File last access time: ",$file->getLastAccessTime(),".<br>";
echo "File lock: ",$file->lock(LOCK_SH);